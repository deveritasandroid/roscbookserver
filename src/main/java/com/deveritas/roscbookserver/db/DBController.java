package com.deveritas.roscbookserver.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBController {

    private static final Logger LOG = Logger.getLogger(DBController.class.getName());
    private Connection connection = null;
    private Statement stmt = null;
    private PreparedStatement pstmt = null;
    private ResultSet rs = null;
    private static String dbUrl = "jdbc:mysql://localhost:13306/roscamestapp";
    private static String dbUser = "root";
    private static String dbPass = "root";

    public DBController() {
    }
    
    public Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            LOG.log(Level.INFO, "Error al registrar el driver: {0}", e.getMessage());
        }
        try {
            connection = DriverManager.getConnection(dbUrl, dbUser, dbPass);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error al conectar con la base de datos: {0}", e.getMessage());
            connection = null;
        }
        return connection;
    }

    public void close() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            LOG.log(Level.INFO, "Error al intentar cerrar un flujo: {0}", e.getMessage());
        }
    }

}
