package com.deveritas.roscbookserver.db;

import com.deveritas.roscbookserver.entidades.Tienda;
import com.deveritas.roscbookserver.entidades.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AppDAO {

    private static final Logger LOG = Logger.getLogger(AppDAO.class.getName());
    private DBController dbController = null;
    private Connection connection = null;
    private PreparedStatement pstmt = null;
    private ResultSet rs = null;

    public AppDAO() {
        dbController = new DBController();
    }

    public List<Usuario> obtenerUsuarios() {
        List<Usuario> usuarios = null;
        try {
            usuarios = new ArrayList<>();
            connection = dbController.getConnection();
            if (connection != null) {
                pstmt = connection.prepareStatement("SELECT * FROM usuario");
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    Usuario temp = new Usuario();
                    temp.setIdusuario(rs.getInt(1));
                    temp.setNombreusuario(rs.getString(2));
                    temp.setPassword(rs.getString(3));
                    usuarios.add(temp);
                }
            } else {
                usuarios = StaticData.getListaUsuarios();
            }
        } catch (Exception e) {
            LOG.log(Level.INFO, "Error al obtener los usuarios de la BD: {0}", e.getMessage());
        }
        return usuarios;
    }

    public List<Tienda> obtenerTiendas() {
        List<Tienda> tiendas = null;
        try {
            tiendas = new ArrayList<>();
            connection = dbController.getConnection();
            if (connection != null) {
                pstmt = connection.prepareStatement("SELECT * FROM tienda");
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    Tienda t = new Tienda();
                    t.setId(rs.getInt(1));
                    t.setNombre(rs.getString(2));
                    t.setUbicacion(rs.getString(3));
                    tiendas.add(t);
                }
            } else {
                tiendas = StaticData.getListaTiendas();
            }

        } catch (Exception e) {
            LOG.log(Level.INFO, "Error al obtener las tiendas de la BD: {0}", e.getMessage());
        }
        return tiendas;
    }

    public Tienda obtenerTiendaPorId(int id) {
        Tienda t = new Tienda();
        try {
            connection = dbController.getConnection();
            if (connection != null) {
                pstmt = connection.prepareStatement("SELECT * FROM tienda WHERE idtienda = ?");
                pstmt.setInt(1, id);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    t.setId(rs.getInt(1));
                    t.setNombre(rs.getString(2));
                    t.setUbicacion(rs.getString(3));
                }
            } else {
                id = id - 1;
                t = StaticData.getListaTiendas().get(id);
            }
        } catch (Exception e) {
            LOG.log(Level.INFO, "Error al obtener la tienda de la BD: {0}", e.getMessage());
        }
        return t;
    }

    public boolean usuarioValido(String user, String pass) {
        boolean res = false;
        try {
            connection = dbController.getConnection();
            pstmt = connection.prepareStatement("SELECT * FROM usuario WHERE nombreusuario = ? AND password = ?");
            pstmt.setString(1, user);
            pstmt.setString(2, pass);
            rs = pstmt.executeQuery();
            res = rs.first();
        } catch (Exception e) {
            LOG.log(Level.INFO, "Error al comunicar con la BD: {0}", e.getMessage());
        }
        return res;
    }

}
