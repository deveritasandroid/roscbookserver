package com.deveritas.roscbookserver.db;

import com.deveritas.roscbookserver.entidades.Evaluacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hsqldb.Server;

public class HSQLDBController {
    
    private static final Logger LOG = Logger.getLogger(HSQLDBController.class.getName());
    private Connection conn = null;
    private Server hsqlServer = null;
    private ResultSet rs = null;
    private Statement st = null;

    public HSQLDBController() {
        initServer();
        initDB();
    }

    private void initServer() {
        hsqlServer = null;
        
        hsqlServer = new Server();
        hsqlServer.setLogWriter(null);
        hsqlServer.setSilent(true);
        hsqlServer.setDatabaseName(0, "evalaucionesdb");
        hsqlServer.setDatabasePath(0, "file:evalaucionesdb");
        
        hsqlServer.start();
        LOG.log(Level.INFO, "HSQLDB server started!");
    }
    
    private void initDB() {
        /*
        try {
            boolean executed = conn.prepareStatement("drop table evaluacion if exists;").execute();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Problema al eliminar la bd: {0}", ex);
        }
                */
        try {
            conn.prepareStatement("CREATE TABLE evaluacion (id integer identity, calificacion integer not null, comentario varchar(50) not null)").execute();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un problema al crear la BD: {0}", ex);
        }
    }
    
    public Connection getConnection() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        try {
            conn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/evalaucionesdb", "sa", "");
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return conn;
    }
    
    public void shutdown() {
        try {
            st = conn.createStatement();
            st.execute("SHUTDOWN");
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        hsqlServer.stop();
    }
    
    public synchronized ResultSet genericQuery(String expression) {
        st = null;
        rs = null;
        
        try {
            st = conn.createStatement();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        try {
            rs = st.executeQuery(expression);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        try {
            st.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    public List<Evaluacion> obtenerEvaluaciones() {
        List<Evaluacion> resultados = null;
        rs = null;
        try {
            resultados = new ArrayList<>();
            rs = conn.prepareStatement("SELECT * FROM evaluacion").executeQuery();
            while (rs.next()) {
                Evaluacion e = new Evaluacion();
                e.setId(rs.getInt(1));
                e.setCalificacion(rs.getInt(2));
                e.setComentario(rs.getString(3));
                resultados.add(e);
            }
        } catch (Exception e) {
            LOG.log(Level.INFO, "Problema al obtener las evaluaciones: {0}", e.getMessage());
        }
        return resultados;
    }
    
    public boolean guardarEvaluacion(Evaluacion e) {
        boolean res = false;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement("INSERT INTO evaluacion(calificacion, comentario) VALUES (?, ?)");
            pstmt.setInt(1, e.getCalificacion());
            pstmt.setString(2, e.getComentario());
            res = pstmt.execute();
        } catch (Exception ex) {
            LOG.log(Level.INFO, "Problema al guardar la evaluaci\u00f3n: {0}", ex.getMessage());
        }
        return res;
    }    
    
    
}