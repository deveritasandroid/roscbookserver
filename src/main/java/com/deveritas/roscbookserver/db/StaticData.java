package com.deveritas.roscbookserver.db;

import com.deveritas.roscbookserver.entidades.Tienda;
import com.deveritas.roscbookserver.entidades.Usuario;
import java.util.ArrayList;
import java.util.List;

public class StaticData {

    private static List<Tienda> listaTiendas = null;
    private static List<Usuario> listaUsuarios = null;

    static {
        listaTiendas = new ArrayList<>();
        listaTiendas.add(new Tienda(1, "Sanborns", "Plaza Carso"));
        listaTiendas.add(new Tienda(2, "Sams Club", "Ejercito Nacional #123"));
        listaTiendas.add(new Tienda(3, "La Esperanza", "Moliere #123"));
        listaTiendas.add(new Tienda(4, "Costo", "Plaza Polanco"));
        listaTiendas.add(new Tienda(5, "Chedraui", "Galerias Polanco"));
        
        listaUsuarios = new ArrayList<>();
        listaUsuarios.add(new Usuario(1, "miguel.rueda", "miguel91"));
    }

    public static List<Tienda> getListaTiendas() {
        return listaTiendas;
    }

    public static List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

}
