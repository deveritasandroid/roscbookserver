package com.deveritas.roscbookserver.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hsqldb.Server;

public class TestDB {
    
    private static final Logger LOG = Logger.getLogger(TestDB.class.getName());
    private Connection conn = null;
    private Server hsqlServer = null;
    private ResultSet rs = null;
    private Statement st = null;

    public TestDB(String db_file_name_prefix) {
        hsqlServer = null;
        
        hsqlServer = new Server();
        hsqlServer.setLogWriter(null);
        hsqlServer.setSilent(true);
        hsqlServer.setDatabaseName(0, "testdb");
        hsqlServer.setDatabasePath(0, "file:testdb");
        
        hsqlServer.start();
        LOG.log(Level.INFO, "HSQLDB server started!");
    }
    
    public Connection getConnection() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        try {
            //conn = DriverManager.getConnection("jdbc:hsqldb:" + db_file_name_prefix, "sa", "");
            conn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/testdb", "sa", "");
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return conn;
    }
    
    public void shutdown() {
        try {
            st = conn.createStatement();
            st.execute("SHUTDOWN");
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
    
    public synchronized void query(String expression) {
        st = null;
        rs = null;
        
        try {
            st = conn.createStatement();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        try {
            rs = st.executeQuery(expression);
            while (rs.next()) {
                Integer id = rs.getInt("ID");
                System.out.println("Id: " + id);
                String strcol = rs.getString("str_col");
                System.out.println("str_col: " + strcol);
                String numcol = rs.getString("num_col");
                System.out.println("num_col: " + numcol);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            dump(rs);
        } catch (SQLException ex) {
            Logger.getLogger(TestDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            // Process result set
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(TestDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public synchronized void update(String expression) {
        st = null;
        try {
            st = conn.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(TestDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        int i = 0;
        try {
            i = st.executeUpdate(expression);
        } catch (SQLException ex) {
            Logger.getLogger(TestDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (i == -1) {
            System.out.println("db error: " + expression);
        }
        try {
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(TestDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void dump(ResultSet rs) throws SQLException {
        ResultSetMetaData meta = null;
        int colmax = 0;
        try {
            meta = rs.getMetaData();
            colmax = meta.getColumnCount();
        } catch (SQLException ex) {
            Logger.getLogger(TestDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        int i;
        Object o = null;
        for(; rs.next(); ) {
            for (i = 0; i < colmax; i++) {
                o = rs.getObject(i + 1);
                System.out.println(o.toString() + " ");
            }
            System.out.println(" ");
        }
    }
    
}
