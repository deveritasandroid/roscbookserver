package com.deveritas.roscbookserver.test;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HSQLTest {
    
    public static void main(String[] args) throws SQLException {
        
        /*
        JDBCDataSource dataSource = new JDBCDataSource();
        dataSource.setDatabaseName("TestDB");
        dataSource.setUser("user");
        dataSource.setPassword("user");
        
        System.out.println("usuario: " + dataSource.getUser());*/
        
        TestDB testDB = null;
        //testDB = new TestDB("db_file");
        testDB = new TestDB("");
        
        //boolean executed = testDB.getConnection().prepareStatement("drop table testtable if exists;").execute();
        //testDB.getConnection().prepareStatement("CREATE TABLE testtable (id integer, name varchar(20) not null)").execute();
        //testDB.getConnection().prepareStatement("INSERT INTO testtable (id, name) VALUES (1, 'Miguel')").execute();
        
        ResultSet rs = testDB.getConnection().prepareStatement("SELECT * FROM testtable;").executeQuery();
        while (rs.next()) {
            System.out.println("id: " + rs.getInt(1) + ", name: " + rs.getString("name"));
        }
        
        /*
        testDB.update("CREATE TABLE sample_table1 (id INTEGER IDENTITY, str_col VARCHAR(256), num_col INTEGER)");
        
        testDB.update("INSERT INTO sample_table1(str_col, num_col) VALUES('Ford', 100)");
        testDB.update("INSERT INTO sample_table1(str_col, num_col) VALUES('Toyota', 200)");
        testDB.update("INSERT INTO sample_table1(str_col, num_col) VALUES('Honda', 300)");
        
        testDB.query("SELECT * FROM sample_table1 WHERE num_col < 250");
                */
        testDB.shutdown();
    }
    
}
