package com.deveritas.roscbookserver.test;

import com.deveritas.roscbookserver.db.AppDAO;
import com.deveritas.roscbookserver.entidades.Tienda;
import java.util.List;

public class TestDAO {

    public static void main(String[] args) {
        AppDAO dao = new AppDAO();

        List<Tienda> listaTiendas = dao.obtenerTiendas();
        for (Tienda tienda : listaTiendas) {
            System.out.println(tienda.toString());
        }

        Tienda t = dao.obtenerTiendaPorId(3);
        System.out.println("Tienda: " + t.toString());
    }

}
