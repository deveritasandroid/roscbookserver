package com.deveritas.roscbookserver.test;

import com.deveritas.roscbookserver.entidades.Evaluacion;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ClientePrueba {

    public static void main(String[] args) {

        Client client = Client.create();

        WebResource webResource = client.resource("http://localhost:10000/evaluar/test");
        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
        if(response.getStatus() != 200) {
            throw new RuntimeException("Error: Código de error: " + response.getStatus());
        }
        
        Evaluacion output = response.getEntity(Evaluacion.class);
        System.out.println("Salida del cliente . . . ");
        System.out.println(output.getId());
        System.out.println(output.getCalificacion());
        System.out.println(output.getComentario());
        
        webResource = client.resource("http://localhost:10000/evaluar/evaluarObjeto");
        response = webResource.accept("application/json").post(ClientResponse.class, output);
        if(response.getStatus() != 200) {
            throw new RuntimeException("Error: " + response.getEntity(String.class));
        }
        System.out.println("Estado: " + response.getStatus());

    }

}

