package com.deveritas.roscbookserver.entidades;

import java.util.logging.Logger;

public class UserCredentials implements java.io.Serializable {

    private static final Logger LOG = Logger.getLogger(UserCredentials.class.getName());
    private static final long serialVersionUID = 1L;
    private String username;
    private String password;

    public UserCredentials() {
    }

    public UserCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserCredentials{" + "username=" + username + ", password=" + password + '}';
    }

}
