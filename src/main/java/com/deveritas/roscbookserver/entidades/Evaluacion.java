package com.deveritas.roscbookserver.entidades;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="evaluacion")
public class Evaluacion implements java.io.Serializable {
    
    private static final long serialVersionUID = 1L;
    private int id;
    private int calificacion;
    private String comentario;

    public Evaluacion() {
    }

    public Evaluacion(int id, int calificacion, String comentario) {
        this.id = id;
        this.calificacion = calificacion;
        this.comentario = comentario;
    }

    public int getId() {
        return id;
    }

    @XmlAttribute
    public void setId(int id) {
        this.id = id;
    }

    public int getCalificacion() {
        return calificacion;
    }

    @XmlElement
    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public String getComentario() {
        return comentario;
    }

    @XmlElement
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
    public String toString() {
        return new StringBuffer("id: ")
                .append(id)
                .append(", calificacion: ")
                .append(calificacion)
                .append(", comentario: ")
                .substring(calificacion);
    }

}
