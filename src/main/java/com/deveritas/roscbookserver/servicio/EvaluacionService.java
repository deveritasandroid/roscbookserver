package com.deveritas.roscbookserver.servicio;

import com.deveritas.roscbookserver.db.AppDAO;
import com.deveritas.roscbookserver.entidades.Evaluacion;
import com.deveritas.roscbookserver.entidades.StatusCode;
import com.deveritas.roscbookserver.entidades.Tienda;
import com.deveritas.roscbookserver.entidades.UserCredentials;
import com.deveritas.roscbookserver.entidades.Usuario;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/evaluar")
public class EvaluacionService {
    
    private static final Logger LOG = Logger.getLogger(EvaluacionService.class.getName());
    private AppDAO appDAO = new AppDAO();
    
    @GET
    @Path("/obtenerUsuarios")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuario> obtenerUsuarios() {
        return appDAO.obtenerUsuarios();
    }
    
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public StatusCode login(UserCredentials credentials) {
        StatusCode status = null;
        LOG.log(Level.INFO, "El usuario: {0} intenta iniciar sesión.", credentials.getUsername());
        if (appDAO.usuarioValido(credentials.getUsername(), credentials.getPassword())) {
            //LOG.log(Level.INFO, "Inicio de sesión exitoso");
            status = new StatusCode(1, "");
        } else {
            //LOG.log(Level.INFO, "Fallo al iniciar sesión");
            status = new StatusCode(-1, "El usuario no es válido");
        }
        return status;
    }
    
    @GET
    @Path("/obtenerTiendas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Tienda> obtenerTiendas() {
        return appDAO.obtenerTiendas();
    }
    
    @GET
    @Path("/obtenerTienda/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Tienda obtenerTiendaPorId(@PathParam("id") int id) {
        return appDAO.obtenerTiendaPorId(id);
    }
    
    @GET
    @Path("/obtenerEvaluaciones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Evaluacion> obtenerEvaluaciones() {
        //return dbController.obtenerEvaluaciones();
        return new ArrayList<>();
    }
    
    @GET
    @Path("/test")
    @Produces(MediaType.APPLICATION_JSON)
    public Evaluacion getEvaluacionPrueba() {
        Evaluacion temp = new Evaluacion();
        temp.setId(5001);
        temp.setCalificacion(5);
        temp.setComentario("La rosca de sanborn's esta muy buena");
        return temp;
    }
    
    @POST
    @Path("/evaluarObjeto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public StatusCode evaluarObjeto(Evaluacion e) {
        if(e == null) {
            return new StatusCode(-1, "La evaluación es nula");
        }
        LOG.log(Level.INFO, "Evaluación de Objeto recibida . . .");
        LOG.log(Level.INFO, String.valueOf(e.getId()));
        LOG.log(Level.INFO, String.valueOf(e.getCalificacion()));
        LOG.log(Level.INFO, e.getComentario());
        return new StatusCode(1, "");
        /*
        boolean res = dbController.guardarEvaluacion(e);
        if (res) {
            return new StatusCode(1, "");
        } else {
            return new StatusCode(-1, "Evaluación no procesada");
        }*/
        
    }
    
}
