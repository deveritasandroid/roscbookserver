package com.deveritas.roscbookserver.server;

import com.sun.jersey.spi.container.servlet.ServletContainer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class GenericServer {

    private static final Logger LOG = Logger.getLogger(GenericServer.class.getName());

    public static void main(String[] args) {
        try {
            ServletHolder servletHolder = new ServletHolder(ServletContainer.class);
            servletHolder.setInitParameter("com.sun.jersey.config.property.resourceConfigClass", "com.sun.jersey.api.core.PackagesResourceConfig");
            servletHolder.setInitParameter("com.sun.jersey.config.property.packages", "com.deveritas.roscbookserver.servicio");
            servletHolder.setInitParameter("com.sun.jersey.api.json.POJOMappingFeature", "true");
            
            Server server = new Server(10000);
            ServletContextHandler ctxHandler = new ServletContextHandler(server, "/");
            ctxHandler.addServlet(servletHolder, "/*");
            server.start();
            server.join();
            LOG.log(Level.INFO, "Servidor iniciado, escuchando en el puerto: 10000");
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un problema con el servidor: {0}", e.getMessage());
        }
    }

}